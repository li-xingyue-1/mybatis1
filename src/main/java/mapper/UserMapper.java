package mapper;
import com.yh.javabean.User;
import java.util.ArrayList;
public interface UserMapper {
    ArrayList<User> selectAll();
    //根据条件查询 那个属性有值查询哪个
    //根据user实际传入的参数查询对应数据
    ArrayList<User> selectByUser(User user);
    //根据条件查询 如果有uid根据uid 否则根据username 否则根据phone
    //只能通过其中一个条件查询 根据设置的优先级匹配
    ArrayList<User> selectByUserOneParam(User user);
    //书写更新方法 根据uid修改其他内容(要求仅修改拥有数值的内容)
    int updateUserByUid(User user);
}
