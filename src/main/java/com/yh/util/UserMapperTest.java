package com.yh.util;

import com.yh.javabean.User;
import mapper.UserMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class UserMapperTest {
    SqlSessionFactory sqlSessionFactory;
    SqlSession sqlSession;
    UserMapper mapper;
    @Before
    public void before() {
        sqlSessionFactory = MyBatisUtil.getSqlSessionFactory();
        sqlSession = sqlSessionFactory.openSession(true);
        mapper = sqlSession.getMapper(UserMapper.class);
    }
    @After
    public void after() {
        sqlSession.close();
    }
    @Test
    public void selectAll(){
        for (User user : mapper.selectAll()) {
            System.out.println(user);
        }
    }
    @Test
    public void selectByUser(){
        User u=new User(1,null,null);
        for (User user : mapper.selectByUser(u)) {
            System.out.println(user);
        }
    }
    @Test
    public void selectByUserOneParam(){
        User u=new User(1,"aaaaaa","1211");
        for (User user : mapper.selectByUserOneParam(u)) {
            System.out.println(user);
        }
    }
    @Test
    public void updateUserByUid(){
        User u=new User(3,null,"bzssb","bzssb");
         mapper.updateUserByUid(u);
    }
}
